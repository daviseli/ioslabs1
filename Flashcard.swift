import Foundation

class Flashcard{
    var term: String = ""
    var definition: String = ""

    init(outTerm:String, outDefinition:String){
        self.term = outTerm
        self.definition = outDefinition
    }

    func createFlashcards()-> Array<Flashcard> {
        var fcard = Flashcard(outTerm: "default", outDefinition: "default")
        var fcardList = Array<Flashcard>()

        for i in 0...9{
            fcard.term = String(i)
            fcard.definition = String(i)

            fcardList.append(Flashcard(outTerm: fcard.term, outDefinition: fcard.definition))
        }

        return fcardList
    }
}