import Foundation

class FlashcardSet{
    var title: String = ""

    init(outTitle:String){
        self.title = outTitle
    }

    func createFlashcards()-> Array<FlashcardSet> {
        var fcardSet = FlashcardSet(outTitle: "default")
        var fcardSetList = Array<FlashcardSet>()

        for i in 0...9{
            fcardSet.title = String(i)

            fcardSetList.append(FlashcardSet(outTitle: fcardSet.title))
        }

        return fcardSetList
    }
}